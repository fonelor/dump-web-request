import { Component, OnInit, OnDestroy } from '@angular/core';
import { CommBusService } from '../comm-bus.service';
import { Subscription } from 'rxjs';
import { LogEvent } from './log-event';

@Component({
  selector: 'app-logs',
  templateUrl: './logs.component.html',
  styleUrls: ['./logs.component.less']
})
export class LogsComponent implements OnInit, OnDestroy {

  commBus: CommBusService;
  logs: LogEvent[] = [];
  subscription: Subscription;

  constructor(commBus: CommBusService) {
    this.commBus = commBus;
  }

  ngOnInit() {
    this.subscription = this.commBus.getLog()
      .subscribe(value => this.printData(value));
    console.log('subscribed');
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
    console.log('unsubscribed');
  }

  printData(logEvent: LogEvent): void {
    this.logs.push(logEvent);
  }

}
