export class LogEvent {
  timestamp: number
  data: string
}
