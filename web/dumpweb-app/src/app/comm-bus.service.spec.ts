import { TestBed } from '@angular/core/testing';

import { CommBusService } from './comm-bus.service';

describe('CommBusService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CommBusService = TestBed.get(CommBusService);
    expect(service).toBeTruthy();
  });
});
