import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {environment} from '../environments/environment';
import {LogEvent} from './logs/log-event';

@Injectable({
  providedIn: 'root'
})
export class CommBusService {

  ws: WebSocket;
  logs: Subject<LogEvent>;

  constructor() {
    this.ws = new WebSocket(environment.wsUrl);

    this.logs = new Subject();
    this.ws.onmessage = ((ev: MessageEvent) => this.onMessage(ev));
    this.ws.onclose = this.onClose;
  }

  private onMessage(event: MessageEvent): void {
    console.log('OnMessage: ' + event.data);
    this.logs.next(JSON.parse(event.data));
  }

  private onClose(event: CloseEvent): void {
    console.log('OnClose: ' + event.code + ' reason ' + event.reason);
  }

  getLog(): Observable<LogEvent> {
    return this.logs;
  }

}
