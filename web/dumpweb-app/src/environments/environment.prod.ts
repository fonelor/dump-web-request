export const environment = {
  production: true,
  wsUrl: 'wss://service-accounts.herokuapp.com/ws/'
};
