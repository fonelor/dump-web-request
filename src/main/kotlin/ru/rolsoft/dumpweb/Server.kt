package ru.rolsoft.dumpweb

import io.ktor.application.Application
import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.features.toLogString
import io.ktor.http.HttpStatusCode
import io.ktor.http.cio.websocket.Frame
import io.ktor.http.content.resource
import io.ktor.http.content.resources
import io.ktor.http.content.static
import io.ktor.request.path
import io.ktor.request.receiveText
import io.ktor.response.respond
import io.ktor.response.respondText
import io.ktor.routing.*
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import io.ktor.websocket.WebSockets
import io.ktor.websocket.webSocket
import java.time.Duration
import java.time.Instant
import java.util.regex.Pattern

/**
 * @author Sergey Filippov <rolenof@gmail.com>
 */

fun main(args: Array<String>) {

    var port = 8080
    if (!System.getProperty("server.port").isNullOrBlank()) {
        port = System.getProperty("server.port").toInt()
    }
    val server = Server()

    embeddedServer(Netty, port = port) {
        server.module(this)
    }.start(wait = true)
}

class Server {
    private val commBus = CommBus()

    private val codeInRequest = Pattern.compile("/(\\d{3})/.*")

    private val logStack = LogStackWithLimit<LogEvent>(100)

    fun module(application: Application) {
        application.install(WebSockets) {
            pingPeriod = Duration.ofSeconds(10)
            timeout = Duration.ofSeconds(5)
        }
        application.routing {
            static {
                resource("dumpweb-app/", "dumpweb-app/index.html")
                static("dumpweb-app/") {
                    resources("dumpweb-app")
                }
            }
            webSocket("ws") {
                logStack.immutableList().forEach { outgoing.send(Frame.Text(it.serialize())) }
                commBus.add(this)
                try {
                    closeReason.await()
                } catch (e: Exception) {
                    commBus.remove(this)
                    println("onClose ${closeReason.await()}")
                }
            }
            get("logs") {
                call.respondText(logStack.immutableList().joinToString("\n") { logEvent -> logEvent.serialize() })
            }
            get("{...}") {
                if ("/favicon.ico" != call.request.path()) {
                    logRequest(call)
                }
            }
            post("{...}") { logRequest(call) }
            put("{...}") { logRequest(call) }
            patch("{...}") { logRequest(call) }
            delete("{...}") { logRequest(call) }
        }

    }

    private suspend fun logRequest(call: ApplicationCall) {
        var codeToAnswer = HttpStatusCode.OK
        val path = call.request.path()
        val matcher = codeInRequest.matcher(path)
        if (matcher.find()) {
            codeToAnswer = HttpStatusCode.fromValue(matcher.group(1).toInt())
        }

        val reqText = "Request ${call.request.toLogString()}. Body: ${call.receiveText()}"
        val event = LogEvent(Instant.now(), reqText)
        logStack.put(event)
        commBus.sendToAll(event.serialize())
        call.respond(codeToAnswer, "Received")
    }
}

