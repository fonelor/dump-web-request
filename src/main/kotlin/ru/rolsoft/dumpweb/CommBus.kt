package ru.rolsoft.dumpweb

import io.ktor.http.cio.websocket.Frame
import io.ktor.http.cio.websocket.WebSocketSession
import java.util.concurrent.CopyOnWriteArrayList

class CommBus {
    private val sessions: MutableList<WebSocketSession> =
        CopyOnWriteArrayList()

    fun add(session: WebSocketSession) = sessions.add(session)

    fun remove(session: WebSocketSession) = sessions.remove(session)

    suspend fun sendToAll(text: String) {
        sessions.forEach {
            try {
                it.outgoing.send(Frame.Text(text))
            } catch (e: Exception) {
                remove(it)
            }
        }
    }
}