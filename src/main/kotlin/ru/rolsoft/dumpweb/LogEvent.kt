package ru.rolsoft.dumpweb

import java.time.Instant

/**
 * @author Sergey Filippov <rolenof@yandex-team.ru>
 */
data class LogEvent(val timestamp: Instant, val data: String) {
    fun serialize(): String =
        """
        {
            "timestamp": ${timestamp.toEpochMilli()},
            "data": "$data"
        }
        """
}