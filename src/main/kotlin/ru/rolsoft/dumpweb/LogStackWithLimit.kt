package ru.rolsoft.dumpweb

import java.util.*
import java.util.concurrent.locks.ReentrantLock

class LogStackWithLimit<T>(private val capacity: Int) {
    private val lock = ReentrantLock()
    private val list: LinkedList<T> = LinkedList<T>()

    fun put(elem: T) {
        lock.lock()
        try {
            if (list.size == capacity) {
                list.removeLast()
            }
            list.addFirst(elem)
        } finally {
            lock.unlock()
        }
    }

    fun immutableList(): List<T> {
        lock.lock()
        try {
            return Collections.unmodifiableList(list.clone() as MutableList<out T>)
        } finally {
            lock.unlock()
        }
    }
}
