import com.liferay.gradle.plugins.node.tasks.ExecuteNpmTask
import com.liferay.gradle.plugins.node.tasks.NpmInstallTask
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import sun.launcher.LauncherHelper
import java.net.URI

plugins {
    kotlin("jvm") version "1.3.10"
    application
    id("com.liferay.node") version "4.5.1"
}

ext {
    set("ktorVersion", "1.0.0-rc")
    set("logbackVersion", "1.2.3")
}

repositories {
    maven { url = URI("https://dl.bintray.com/kotlin/ktor") }
}

application {
    mainClassName = "ru.rolsoft.dumpweb.ServerKt"
}

node {
    setNodeDir("web/dumpweb-app/node")
    setNodeVersion("10.7.0")
}

group = "ru.rolsoft"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    compile(kotlin("stdlib-jdk8"))

    compile("ch.qos.logback:logback-classic:${project.extra["logbackVersion"]}")

    compile("io.ktor:ktor-server-netty:${project.extra["ktorVersion"]}")
    compile("io.ktor:ktor-websockets:${project.extra["ktorVersion"]}")
}

val webRoot = "web/dumpweb-app"

tasks {
    withType<KotlinCompile> {
        kotlinOptions.jvmTarget = "1.8"
    }

    val npmInstall = create<ExecuteNpmTask>("npmInstallMy") {
        setCacheDir("$webRoot/node_modules")
        setWorkingDir(webRoot)
        args.addAll(listOf("install"))
    }

    val buildWeb = create<ExecuteNpmTask>("buildWeb") {
        dependsOn(npmInstall)
        setCacheDir("$webRoot/node_modules")
        setWorkingDir(webRoot)
        args.addAll(listOf("run-script", "build-prod"))
    }

    val fatJar = create<Jar>("fatJar") {
        dependsOn(buildWeb)
        baseName = "${project.name}-fat"
        manifest {
            attributes["Main-Class"] = "ru.rolsoft.dumpweb.ServerKt"
        }
        from(
            configurations.runtime.map {
                if (it.isDirectory) it else zipTree(it)
            }
        )
        from("$webRoot/dist")
        with(tasks["jar"] as CopySpec)
    }

    create<Task>("stage") {
        dependsOn(fatJar)
    }
}

