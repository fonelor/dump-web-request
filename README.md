# Dump web request

**build:** `gradle fatJar`

**run:** `java -Dserver.port=$PORT -jar build/libs/dumpweb-fat-1.0-SNAPSHOT.jar`

## Debug

1. Start angular debug server: `cd web/dumpweb-app && npm run-script start`
2. Debug kotlin app in your ide
3. Web app will be on **http://localhost:4200/**
